package lt.vtvpmc.exam.entities;

public enum ClientType {
	SIMPLE("Simple"),
	LOYAL("Loyal");
	
	private String value;
	
	private ClientType(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
