package lt.vtvpmc.exam.ui;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.ClientType;



public class NewClientBean implements Serializable {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private String clientFirstName;
    private String clientLastName;
    private String clientPhoneNumber;
    private Date dateOfBirth;
    private ClientType clientType;
    
    
    @Transactional
    public String save() {
    	if (getClientByNameAndDate(clientFirstName, clientLastName, dateOfBirth).size() == 0){
    		Client client = new Client(clientFirstName, clientLastName, clientPhoneNumber, dateOfBirth, clientType);
    		entityManager.persist(client);
    		return "main";
        } else {
        	return "error";
        }
    }
    
    @Transactional
    public List<Client> getClientByNameAndDate(String firstName, String lastName, Date dateOfBirth) {
        Query q = entityManager.createQuery("select c from Client c where c.firstName = :firstName and c.lastName = :lastName and c.dateOfBirth = :dateOfBirth");
        q.setParameter("firstName", firstName);
        q.setParameter("lastName", lastName);
        q.setParameter("dateOfBirth", dateOfBirth);
        return q.getResultList();
    }
    
    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public void setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
    }

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public ClientType getClientType() {
		return clientType;
	}

	public void setClientType(ClientType clientType) {
		this.clientType = clientType;
	}
}
