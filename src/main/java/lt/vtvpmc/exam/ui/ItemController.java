package lt.vtvpmc.exam.ui;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import lt.vtvpmc.exam.entities.Item;

public class ItemController implements Serializable {
	
	private ItemModel itemModel;
	private ClientModel clientModel;
	
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public String save() {
		if (!clientModel.getCurrentClient().getItems().contains(itemModel.getCurrentItem())) {
			clientModel.getCurrentClient().getItems().add(itemModel.getCurrentItem());
		}
		em.persist(itemModel.getCurrentItem());
		return "client";
	}

	@Transactional
	public String delete(Item item) {
		clientModel.getCurrentClient().getItems().remove(item);
		em.remove(em.merge(item));
		return "client";
	}
	
	public String create(){
		Item newItem = new Item();
		newItem.setClient(clientModel.getCurrentClient());
		itemModel.setCurrentItem(newItem);
		return "addItem";
	}
	
	public String cancel() {
		itemModel.setCurrentItem(null);
		return "client";
	}
	
	public ItemModel getItemModel() {
		return itemModel;
	}

	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}
}
