package lt.vtvpmc.exam.ui;

import java.io.Serializable;

import lt.vtvpmc.exam.entities.Item;

public class ItemModel implements Serializable {
	private Item currentItem;

	public Item getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(Item currentItem) {
		this.currentItem = currentItem;
	}
}
