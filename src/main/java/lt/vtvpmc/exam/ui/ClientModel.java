package lt.vtvpmc.exam.ui;

import java.io.Serializable;

import lt.vtvpmc.exam.entities.Client;

public class ClientModel implements Serializable {
	private Client currentClient;

	public Client getCurrentClient() {
		return currentClient;
	}

	public void setCurrentClient(Client currentClient) {
		this.currentClient = currentClient;
	}
}
