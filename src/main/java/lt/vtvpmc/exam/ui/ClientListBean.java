
package lt.vtvpmc.exam.ui;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Item;

/**
 *
 * @author giedrius
 */
public class ClientListBean implements Serializable {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private ClientModel clientModel;
    
    @Transactional(readOnly = true)
    public List<Client> getClientList() {
        Query q = entityManager.createQuery("select c from Client c");
        return q.getResultList();
    }
    
    @Transactional
    public void removeClient(Client client) {
        entityManager.remove(entityManager.merge(client));
    }
    
    public Integer getNumberOfInventory(Client client)
    {
		TypedQuery<Item> query = entityManager.createQuery("SELECT t From Item t WHERE t.client = :client", Item.class);
		query.setParameter("client", client);
		return query.getResultList().size();
    }
    
	public String overview(Client client) {
		clientModel.setCurrentClient(client);
		return "client";
	}

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}
    
}
