package lt.vtvpmc.exam.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import lt.vtvpmc.exam.entities.Client;
import lt.vtvpmc.exam.entities.Item;

public class ReportBean implements Serializable {

	private ClientListBean clientListBean;
	@PersistenceContext
	private EntityManager entityManager;

	public Integer getWeightSum(Client client) {
		int sum = 0;
		for (Item item: client.getItems()){
			sum += item.getWeight();
		}
		return sum;
	}
	
    @Transactional(readOnly = true)
    public List<Client> getClientListByItemCount() {
    	Query query = entityManager.createQuery("SELECT c From Client c order by size(c.items) DESC").setMaxResults(5);
    	return query.getResultList();
    }
    
    public List<Client> getClientListByWeigth()
    {
    	List<Client> filteredClients = new ArrayList<Client>();
    	List<Client> allClients = clientListBean.getClientList();
    	for (int i=0; i<5; i++){
    		if (i< allClients.size()){
    			filteredClients.add(allClients.get(i));
    		}
    	}
    	for (int j=0; j<allClients.size(); j++){
    		
    		for (int i=0; i<5; i++){
    			if (getWeightSum(allClients.get(j)) > getWeightSum(filteredClients.get(i))){
    		
    				if (!filteredClients.contains(allClients.get(j))){
        				filteredClients.set(i, allClients.get(j));
    				}
    				break;
    			}
    		}
    	}
    	return filteredClients;
    }
    


	public ClientListBean getClientListBean() {
		return clientListBean;
	}

	public void setClientListBean(ClientListBean clientListBean) {
		this.clientListBean = clientListBean;
	}
    
    

}
